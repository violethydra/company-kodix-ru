/* ==== IMPORT PARAMS ==== */

/* ==== ----- ==== */

/* ==== Sources and directions for files ==== */
// const	inDev = 'development';
const	inPub = 'public';
/* ==== ----- ==== */

module.exports = (nameTask, _run, combiner, src, dest, isDevelopment, isPublic, errorConfig) =>
	() => combiner(

		src(`${inPub}/*.html`),
		_run.if(isPublic, _run.htmlmin({ collapseWhitespace: true })),
		_run.rename({ suffix: '.min' }),
		dest(inPub)

	).on('error',
		_run.notify.onError((err) => errorConfig(`task: ${nameTask} `, 'ошибка!', err)));
