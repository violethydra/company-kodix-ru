module.exports = class AddPhoneInputMask {
	constructor(init) {
		this.selector = init.selector;
		this.input = init.inputid;
	}

	static info() {
		console.log('MODULE:', this.name, true);
	}

	run() {
		const elem = document.querySelector(`${this.selector}`);
		if (elem) {
			this.constructor.info();
			const select = document.querySelector(`${this.input}`);
			const myfunc = (event) => {
				const regexp = /^\+?[0-9]{0,1}\s?[(]{0,1}[0-9]{0,3}[)]{0,1}[-\s./0-9]*$/;
				if (!select.value.match(regexp)) select.value = select.value.slice(0, -1);
			};

			select.addEventListener('keyup', event => myfunc(event));
		}
	}
};
