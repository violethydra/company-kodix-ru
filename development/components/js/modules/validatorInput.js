module.exports = class AddValidatorInput {
	constructor(init) {
		this.server = init.server;
		this.selector = init.selector;
		this.submitValid = false;
		this.errorText = '💀 Ouch, database error...';
	}

	static info() {
		console.log('MODULE:', this.name, true);
	}

	run() {
		const elem = document.querySelector(`${this.selector}`);
		if (elem) {
			this.constructor.info();

			const tools = document.querySelector('#tools-select');
			const toolsInput = () => document.querySelectorAll('.tools__input.warning');
			const button = document.querySelector('#tools-form button');
			const inputs = document.querySelectorAll('#tools-form input');
			const selectList = () => document.querySelectorAll('.dropdown__select li');

			const filterEmpty = (item) => {
				if (item.value.length <= 2) item.classList.add('warning');
			};

			const nextSubmit = () => {
				console.log('Отправлено!');
				[...document.querySelectorAll('.tools__input')].forEach((el) => {
					const currentElem = el;
					currentElem.style.backgroundColor = '#ddd';
					currentElem.style.pointerEvents = 'none';
					currentElem.setAttribute('readonly', true);
				});

				const inputButtonChange = document.querySelector('.tools__button');
				inputButtonChange.style.pointerEvents = 'none';
				inputButtonChange.style.backgroundColor = '#5ccc2c';
				inputButtonChange.innerText = 'Отправлено!';
				inputButtonChange.setAttribute('readonly', true);
			};

			const clickToMyButton = (event) => {
				this.submitValid = false;
				Array.from(inputs).filter(filterEmpty).map(el => el);
				if (toolsInput().length === 0) this.submitValid = true;
				if (this.submitValid) nextSubmit();
			};

			const changeMyInput = (event) => {
				if (event.target.value.length >= 2) event.target.classList.remove('warning');
			};

			const delayFunc = x => Array.from(x).map((el) => {
				el.addEventListener('click', event => tools.classList.remove('warning'));
				return false;
			});

			setTimeout(() => delayFunc(selectList()), 1000);

			button.addEventListener('click', event => clickToMyButton(event));
			Array.from(inputs).forEach(el => el.addEventListener('input', event => changeMyInput(event)));
		}
	}
};
