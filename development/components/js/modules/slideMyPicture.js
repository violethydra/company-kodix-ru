module.exports = class AddSlideMyPicture {
	constructor(init) {
		this.server = init.server;
		this.selector = init.selector;
		this.indexNav = 0;
		this.indexNavMin = 0;
		this.indexNavMax = 0;
		this.timerInterval = null;
		this.errorText = '💀 Ouch, database error...';
	}

	static info() {
		console.log('MODULE:', this.name, true);
	}

	run() {
		const elem = document.querySelector(`${this.selector}`);
		if (elem) {
			this.constructor.info();

			const selector = document.querySelector('.js__slideMyPictureNavbar');
			const myRemove = document.querySelector('.js__slideMyPictureNavbar span');
			const buttonRight = document.querySelector('.gallery__arrow-right');
			const buttonLeft = document.querySelector('.gallery__arrow-left');
			const clickPlay = document.querySelector('.gallery__play-button');
			const stopClicklPlay = document.querySelector('.gallery__play-stopper');

			if (myRemove.textContent.includes(this.errorText)) myRemove.remove();

			this.indexNavMax = Array.from(document.querySelectorAll('.js__slideMyPicture IMG')).length - 1;

			Array.from(document.querySelectorAll('.js__slideMyPicture IMG')).map((el) => {
				const createDIV = document.createElement('DIV');
				createDIV.classList.add('gallery__navbar-dots');
				createDIV.innerHTML += '&nbsp;';
				return selector.appendChild(createDIV);
			});

			selector.firstElementChild.classList.add('active');

			const logic = (event) => {
				killMyTimer(this.timerInterval);
				const curElem = event.target;
				this.indexNav = [...curElem.parentElement.children].indexOf(curElem);
				if (curElem.classList.contains('active')) return false;
				if (selector.querySelector('.active')) {
					selector.querySelector('.active').classList.remove('active');
				}
				currentIndex();
				return selector.children[this.indexNav].classList.add('active');
			};

			const MoveNavDots = () => {
				selector.querySelector('.active').classList.remove('active');
				selector.children[this.indexNav].classList.add('active');
			};
			
			const countIndexClassList = () => {
				elem.querySelector('.active').classList.remove('active');
				if (elem.children[this.indexNav].classList.contains('active')) {
					elem.children[this.indexNav].remove('active');
				}
			};
			
			const currentIndex = () => {
				countIndexClassList();
				elem.children[this.indexNav].classList.add('active');
			};

			const moveInRight = () => {
				if (this.indexNav >= this.indexNavMax) return false;
				countIndexClassList();
				elem.children[this.indexNav += 1].classList.add('active');
				return MoveNavDots();
			};

			const moveInLeft = () => {
				if (this.indexNav <= this.indexNavMin) return false;
				countIndexClassList();
				elem.children[this.indexNav += -1].classList.add('active');
				return MoveNavDots();
			};

			const timerFnc = () => {
				if (this.indexNav >= this.indexNavMax) {
					this.indexNav = 1;
					moveInLeft();
				} else moveInRight();
			};

			clickPlay.addEventListener('click', () => {
				clickPlay.style.opacity = 0;
				clickPlay.style.visibility = 'hidden';
				stopClicklPlay.style.visibility = 'visible';
				this.timerInterval = setInterval(() => timerFnc(), 1000);
			});

			const killMyTimer = (x) => {
				clearInterval(x);
				clickPlay.style.opacity = 1;
				clickPlay.style.visibility = 'visible';
				stopClicklPlay.style.visibility = '';
			};

			stopClicklPlay.addEventListener('click', () => killMyTimer(this.timerInterval));
			selector.addEventListener('click', event => logic(event));
			buttonRight.addEventListener('click', () => moveInRight());
			buttonLeft.addEventListener('click', () => moveInLeft());

		}
	}
};
