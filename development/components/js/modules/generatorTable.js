module.exports = class AddGeneratorTable {
	constructor(init) {
		this.server = init.server;
		this.selector = init.selector;
		this.sortCar = document.querySelector('.tools__sort-car');
		this.sortHeader = document.querySelector('.tools__sort-header');
		this.errorText = '💀 Ouch, database error...';
	}

	static info() {
		console.log('MODULE:', this.name, true);
	}

	run() {
		const elem = document.querySelector(`${this.selector}`);
		if (elem) {
			this.constructor.info();

			const init = {
				method: 'GET',
				headers: { 'Content-Type': 'application/json' },
				mode: 'cors',
				cache: 'default'
			};

			fetch(`./${this.server}`, init)
				.then((response) => {
					response.headers.get('Content-Type');
					return response.json();
				})
				.then((data) => {
					const myRemove = [this.sortCar, this.sortHeader];
					myRemove.forEach(el => (el.textContent.includes(this.errorText) ? el.remove() : ''));

					Object.values(data[0].column).map((i, index) => {
						const creatDIV = document.createElement('DIV');
						creatDIV.className = 'tools__sort-header';
						creatDIV.innerHTML += i.title;
						elem.querySelector('.tools__sort-title').appendChild(creatDIV);
						return false;
					});

					Object.values(data.slice(1)).forEach((i, index) => {
						const createElemUX = document.createElement('DIV');
						createElemUX.className = 'tools__sort-car tools__sort-line';

						Object.values(i.car).forEach((f, fIndex) => {
							const createElemDIV = document.createElement('DIV');
							createElemDIV.className = 'tools__sort-item';
							createElemDIV.innerHTML += f;
							createElemUX.appendChild(createElemDIV);
							return false;
						});

						elem.appendChild(createElemUX);
					});
				})
				.then(() => {
					const droplist = document.querySelector('.js__createDropList');
					const selectUl = document.createElement('UL');
					selectUl.className = 'dropdown__item dropdown__select';

					[...document.querySelectorAll('.tools__sort-car')].forEach((f, index) => {
						const selectLi = document.createElement('LI');
						selectUl.appendChild(selectLi);
						selectLi.innerHTML += f.children[0].textContent;
					});
					droplist.appendChild(selectUl);

				})
				.then(() => {
					const selector = document.querySelector('MAIN');
					selector.addEventListener('click', (event) => {

						document.querySelector('.js__createDropList UL').classList.remove('open');
						if (event.target.matches('.dropdown__label')) {
							event.preventDefault() && event.stopPropagation();
						}
						if (event.target.matches('.js__createDropList INPUT')) {
							event.target.parentElement.querySelector('UL').classList.add('open');
						}
						if (event.target.nodeName === 'LI') {
							const grabText = event.target.textContent;
							const pasteInSelector = event.target.parentElement.parentElement;
							pasteInSelector.querySelector('INPUT').value = grabText || '';
						}

					});
				})
				.catch(error => console.log(`Ouch! Fetch error: \n --> ${error.message}`));
		}
	}
};
