// ============================
//    Name: index.js
// ============================

import AddMoveScrollUP from './modules/moveScrollUP';
import AddGeneratorTable from './modules/generatorTable';
import AddPhoneInputMask from './modules/phoneInputMask';
import AddValidatorInput from './modules/validatorInput';
import AddSlideMyPicture from './modules/slideMyPicture';
import AddOpenMyBurger from './modules/openMyBurger';

const start = () => {
	console.log('DOM:', 'DOMContentLoaded', true);
	
	new AddGeneratorTable({
		server: 'equipment.json',
		selector: '.js__sortTable'
	}).run();
	
	new AddPhoneInputMask({
		selector: '[data-phone="true"]',
		inputid: '#tools-phone'
	}).run();
	
	new AddValidatorInput({
		selector: '#tools-form'
	}).run();
	
	new AddSlideMyPicture({
		selector: '.js__slideMyPicture'
	}).run();
	
	new AddOpenMyBurger({
		burger: '.js__navHamburger',
		navbar: '.js__navHamburgerOpener'
	}).run();
	
	new AddMoveScrollUP('.js__moveScrollUP').run();
	
};

if (typeof window !== 'undefined' && window && window.addEventListener) {
	document.addEventListener('DOMContentLoaded', start(), false);
}
 