/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./development/components/js/connect.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./development/components/js/connect.js":
/*!**********************************************!*\
  !*** ./development/components/js/connect.js ***!
  \**********************************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _modules_moveScrollUP__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./modules/moveScrollUP */ "./development/components/js/modules/moveScrollUP.js");
/* harmony import */ var _modules_moveScrollUP__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_modules_moveScrollUP__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _modules_generatorTable__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./modules/generatorTable */ "./development/components/js/modules/generatorTable.js");
/* harmony import */ var _modules_generatorTable__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_modules_generatorTable__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _modules_phoneInputMask__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./modules/phoneInputMask */ "./development/components/js/modules/phoneInputMask.js");
/* harmony import */ var _modules_phoneInputMask__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_modules_phoneInputMask__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _modules_validatorInput__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./modules/validatorInput */ "./development/components/js/modules/validatorInput.js");
/* harmony import */ var _modules_validatorInput__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_modules_validatorInput__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _modules_slideMyPicture__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./modules/slideMyPicture */ "./development/components/js/modules/slideMyPicture.js");
/* harmony import */ var _modules_slideMyPicture__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_modules_slideMyPicture__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _modules_openMyBurger__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./modules/openMyBurger */ "./development/components/js/modules/openMyBurger.js");
/* harmony import */ var _modules_openMyBurger__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(_modules_openMyBurger__WEBPACK_IMPORTED_MODULE_5__);
// ============================
//    Name: index.js
// ============================







var start = function start() {
  console.log('DOM:', 'DOMContentLoaded', true);
  new _modules_generatorTable__WEBPACK_IMPORTED_MODULE_1___default.a({
    server: 'equipment.json',
    selector: '.js__sortTable'
  }).run();
  new _modules_phoneInputMask__WEBPACK_IMPORTED_MODULE_2___default.a({
    selector: '[data-phone="true"]',
    inputid: '#tools-phone'
  }).run();
  new _modules_validatorInput__WEBPACK_IMPORTED_MODULE_3___default.a({
    selector: '#tools-form'
  }).run();
  new _modules_slideMyPicture__WEBPACK_IMPORTED_MODULE_4___default.a({
    selector: '.js__slideMyPicture'
  }).run();
  new _modules_openMyBurger__WEBPACK_IMPORTED_MODULE_5___default.a({
    burger: '.js__navHamburger',
    navbar: '.js__navHamburgerOpener'
  }).run();
  new _modules_moveScrollUP__WEBPACK_IMPORTED_MODULE_0___default.a('.js__moveScrollUP').run();
};

if (typeof window !== 'undefined' && window && window.addEventListener) {
  document.addEventListener('DOMContentLoaded', start(), false);
}

/***/ }),

/***/ "./development/components/js/modules/generatorTable.js":
/*!*************************************************************!*\
  !*** ./development/components/js/modules/generatorTable.js ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance"); }

function _iterableToArray(iter) { if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === "[object Arguments]") return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

module.exports =
/*#__PURE__*/
function () {
  function AddGeneratorTable(init) {
    _classCallCheck(this, AddGeneratorTable);

    this.server = init.server;
    this.selector = init.selector;
    this.sortCar = document.querySelector('.tools__sort-car');
    this.sortHeader = document.querySelector('.tools__sort-header');
    this.errorText = '💀 Ouch, database error...';
  }

  _createClass(AddGeneratorTable, [{
    key: "run",
    value: function run() {
      var _this = this;

      var elem = document.querySelector("".concat(this.selector));

      if (elem) {
        this.constructor.info();
        var init = {
          method: 'GET',
          headers: {
            'Content-Type': 'application/json'
          },
          mode: 'cors',
          cache: 'default'
        };
        fetch("./".concat(this.server), init).then(function (response) {
          response.headers.get('Content-Type');
          return response.json();
        }).then(function (data) {
          var myRemove = [_this.sortCar, _this.sortHeader];
          myRemove.forEach(function (el) {
            return el.textContent.includes(_this.errorText) ? el.remove() : '';
          });
          Object.values(data[0].column).map(function (i, index) {
            var creatDIV = document.createElement('DIV');
            creatDIV.className = 'tools__sort-header';
            creatDIV.innerHTML += i.title;
            elem.querySelector('.tools__sort-title').appendChild(creatDIV);
            return false;
          });
          Object.values(data.slice(1)).forEach(function (i, index) {
            var createElemUX = document.createElement('DIV');
            createElemUX.className = 'tools__sort-car tools__sort-line';
            Object.values(i.car).forEach(function (f, fIndex) {
              var createElemDIV = document.createElement('DIV');
              createElemDIV.className = 'tools__sort-item';
              createElemDIV.innerHTML += f;
              createElemUX.appendChild(createElemDIV);
              return false;
            });
            elem.appendChild(createElemUX);
          });
        }).then(function () {
          var droplist = document.querySelector('.js__createDropList');
          var selectUl = document.createElement('UL');
          selectUl.className = 'dropdown__item dropdown__select';

          _toConsumableArray(document.querySelectorAll('.tools__sort-car')).forEach(function (f, index) {
            var selectLi = document.createElement('LI');
            selectUl.appendChild(selectLi);
            selectLi.innerHTML += f.children[0].textContent;
          });

          droplist.appendChild(selectUl);
        }).then(function () {
          var selector = document.querySelector('MAIN');
          selector.addEventListener('click', function (event) {
            document.querySelector('.js__createDropList UL').classList.remove('open');

            if (event.target.matches('.dropdown__label')) {
              event.preventDefault() && event.stopPropagation();
            }

            if (event.target.matches('.js__createDropList INPUT')) {
              event.target.parentElement.querySelector('UL').classList.add('open');
            }

            if (event.target.nodeName === 'LI') {
              var grabText = event.target.textContent;
              var pasteInSelector = event.target.parentElement.parentElement;
              pasteInSelector.querySelector('INPUT').value = grabText || '';
            }
          });
        }).catch(function (error) {
          return console.log("Ouch! Fetch error: \n --> ".concat(error.message));
        });
      }
    }
  }], [{
    key: "info",
    value: function info() {
      console.log('MODULE:', this.name, true);
    }
  }]);

  return AddGeneratorTable;
}();

/***/ }),

/***/ "./development/components/js/modules/moveScrollUP.js":
/*!***********************************************************!*\
  !*** ./development/components/js/modules/moveScrollUP.js ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

module.exports =
/*#__PURE__*/
function () {
  function AddMoveScrollUP(item, speed) {
    _classCallCheck(this, AddMoveScrollUP);

    this.item = item;
    this.speed = speed;
  }

  _createClass(AddMoveScrollUP, [{
    key: "run",
    value: function run() {
      var _this = this;

      var elem = document.querySelector("".concat(this.item));

      if (elem) {
        this.constructor.info();
        this.speed = this.speed === 'fast' ? 8 / 2 : 8;
        this.speed = this.speed === 'slow' ? 8 * 2 : 8;

        var scrollMe = function scrollMe() {
          var getScroll = document.documentElement.scrollTop || document.body.scrollTop;

          if (getScroll > 0) {
            window.requestAnimationFrame(scrollMe);
            window.scrollTo(0, getScroll - getScroll / _this.speed);
          }
        };

        elem.addEventListener('click', function () {
          return scrollMe();
        });
      }
    }
  }], [{
    key: "info",
    value: function info() {
      console.log('MODULE:', this.name, true);
    }
  }]);

  return AddMoveScrollUP;
}();

/***/ }),

/***/ "./development/components/js/modules/openMyBurger.js":
/*!***********************************************************!*\
  !*** ./development/components/js/modules/openMyBurger.js ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

module.exports =
/*#__PURE__*/
function () {
  function AddOpenMyBurger(init) {
    _classCallCheck(this, AddOpenMyBurger);

    this.selector = init.burger;
    this.navbar = init.navbar;
    this.errorText = '💀 Ouch, database error...';
  }

  _createClass(AddOpenMyBurger, [{
    key: "run",
    value: function run() {
      var _this = this;

      var selector = document.querySelector("".concat(this.selector));

      if (selector) {
        this.constructor.info();

        var openBurger = function openBurger() {
          var nav = document.querySelector("".concat(_this.navbar));
          nav.querySelector('DIV').classList.toggle('open');
          selector.classList.toggle('open');
        };

        selector.addEventListener('click', function () {
          return openBurger();
        });
      }
    }
  }], [{
    key: "info",
    value: function info() {
      console.log('MODULE:', this.name, true);
    }
  }]);

  return AddOpenMyBurger;
}();

/***/ }),

/***/ "./development/components/js/modules/phoneInputMask.js":
/*!*************************************************************!*\
  !*** ./development/components/js/modules/phoneInputMask.js ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

module.exports =
/*#__PURE__*/
function () {
  function AddPhoneInputMask(init) {
    _classCallCheck(this, AddPhoneInputMask);

    this.selector = init.selector;
    this.input = init.inputid;
  }

  _createClass(AddPhoneInputMask, [{
    key: "run",
    value: function run() {
      var elem = document.querySelector("".concat(this.selector));

      if (elem) {
        this.constructor.info();
        var select = document.querySelector("".concat(this.input));

        var myfunc = function myfunc(event) {
          var regexp = /^\+?[0-9]{0,1}\s?[(]{0,1}[0-9]{0,3}[)]{0,1}[-\s./0-9]*$/;
          if (!select.value.match(regexp)) select.value = select.value.slice(0, -1);
        };

        select.addEventListener('keyup', function (event) {
          return myfunc(event);
        });
      }
    }
  }], [{
    key: "info",
    value: function info() {
      console.log('MODULE:', this.name, true);
    }
  }]);

  return AddPhoneInputMask;
}();

/***/ }),

/***/ "./development/components/js/modules/slideMyPicture.js":
/*!*************************************************************!*\
  !*** ./development/components/js/modules/slideMyPicture.js ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance"); }

function _iterableToArray(iter) { if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === "[object Arguments]") return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

module.exports =
/*#__PURE__*/
function () {
  function AddSlideMyPicture(init) {
    _classCallCheck(this, AddSlideMyPicture);

    this.server = init.server;
    this.selector = init.selector;
    this.indexNav = 0;
    this.indexNavMin = 0;
    this.indexNavMax = 0;
    this.timerInterval = null;
    this.errorText = '💀 Ouch, database error...';
  }

  _createClass(AddSlideMyPicture, [{
    key: "run",
    value: function run() {
      var _this = this;

      var elem = document.querySelector("".concat(this.selector));

      if (elem) {
        this.constructor.info();
        var selector = document.querySelector('.js__slideMyPictureNavbar');
        var myRemove = document.querySelector('.js__slideMyPictureNavbar span');
        var buttonRight = document.querySelector('.gallery__arrow-right');
        var buttonLeft = document.querySelector('.gallery__arrow-left');
        var clickPlay = document.querySelector('.gallery__play-button');
        var stopClicklPlay = document.querySelector('.gallery__play-stopper');
        if (myRemove.textContent.includes(this.errorText)) myRemove.remove();
        this.indexNavMax = Array.from(document.querySelectorAll('.js__slideMyPicture IMG')).length - 1;
        Array.from(document.querySelectorAll('.js__slideMyPicture IMG')).map(function (el) {
          var createDIV = document.createElement('DIV');
          createDIV.classList.add('gallery__navbar-dots');
          createDIV.innerHTML += '&nbsp;';
          return selector.appendChild(createDIV);
        });
        selector.firstElementChild.classList.add('active');

        var logic = function logic(event) {
          killMyTimer(_this.timerInterval);
          var curElem = event.target;
          _this.indexNav = _toConsumableArray(curElem.parentElement.children).indexOf(curElem);
          if (curElem.classList.contains('active')) return false;

          if (selector.querySelector('.active')) {
            selector.querySelector('.active').classList.remove('active');
          }

          currentIndex();
          return selector.children[_this.indexNav].classList.add('active');
        };

        var MoveNavDots = function MoveNavDots() {
          selector.querySelector('.active').classList.remove('active');

          selector.children[_this.indexNav].classList.add('active');
        };

        var countIndexClassList = function countIndexClassList() {
          elem.querySelector('.active').classList.remove('active');

          if (elem.children[_this.indexNav].classList.contains('active')) {
            elem.children[_this.indexNav].remove('active');
          }
        };

        var currentIndex = function currentIndex() {
          countIndexClassList();

          elem.children[_this.indexNav].classList.add('active');
        };

        var moveInRight = function moveInRight() {
          if (_this.indexNav >= _this.indexNavMax) return false;
          countIndexClassList();
          elem.children[_this.indexNav += 1].classList.add('active');
          return MoveNavDots();
        };

        var moveInLeft = function moveInLeft() {
          if (_this.indexNav <= _this.indexNavMin) return false;
          countIndexClassList();
          elem.children[_this.indexNav += -1].classList.add('active');
          return MoveNavDots();
        };

        var timerFnc = function timerFnc() {
          if (_this.indexNav >= _this.indexNavMax) {
            _this.indexNav = 1;
            moveInLeft();
          } else moveInRight();
        };

        clickPlay.addEventListener('click', function () {
          clickPlay.style.opacity = 0;
          clickPlay.style.visibility = 'hidden';
          stopClicklPlay.style.visibility = 'visible';
          _this.timerInterval = setInterval(function () {
            return timerFnc();
          }, 1000);
        });

        var killMyTimer = function killMyTimer(x) {
          clearInterval(x);
          clickPlay.style.opacity = 1;
          clickPlay.style.visibility = 'visible';
          stopClicklPlay.style.visibility = '';
        };

        stopClicklPlay.addEventListener('click', function () {
          return killMyTimer(_this.timerInterval);
        });
        selector.addEventListener('click', function (event) {
          return logic(event);
        });
        buttonRight.addEventListener('click', function () {
          return moveInRight();
        });
        buttonLeft.addEventListener('click', function () {
          return moveInLeft();
        });
      }
    }
  }], [{
    key: "info",
    value: function info() {
      console.log('MODULE:', this.name, true);
    }
  }]);

  return AddSlideMyPicture;
}();

/***/ }),

/***/ "./development/components/js/modules/validatorInput.js":
/*!*************************************************************!*\
  !*** ./development/components/js/modules/validatorInput.js ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance"); }

function _iterableToArray(iter) { if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === "[object Arguments]") return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

module.exports =
/*#__PURE__*/
function () {
  function AddValidatorInput(init) {
    _classCallCheck(this, AddValidatorInput);

    this.server = init.server;
    this.selector = init.selector;
    this.submitValid = false;
    this.errorText = '💀 Ouch, database error...';
  }

  _createClass(AddValidatorInput, [{
    key: "run",
    value: function run() {
      var _this = this;

      var elem = document.querySelector("".concat(this.selector));

      if (elem) {
        this.constructor.info();
        var tools = document.querySelector('#tools-select');

        var toolsInput = function toolsInput() {
          return document.querySelectorAll('.tools__input.warning');
        };

        var button = document.querySelector('#tools-form button');
        var inputs = document.querySelectorAll('#tools-form input');

        var selectList = function selectList() {
          return document.querySelectorAll('.dropdown__select li');
        };

        var filterEmpty = function filterEmpty(item) {
          if (item.value.length <= 2) item.classList.add('warning');
        };

        var nextSubmit = function nextSubmit() {
          console.log('Отправлено!');

          _toConsumableArray(document.querySelectorAll('.tools__input')).forEach(function (el) {
            var currentElem = el;
            currentElem.style.backgroundColor = '#ddd';
            currentElem.style.pointerEvents = 'none';
            currentElem.setAttribute('readonly', true);
          });

          var inputButtonChange = document.querySelector('.tools__button');
          inputButtonChange.style.pointerEvents = 'none';
          inputButtonChange.style.backgroundColor = '#5ccc2c';
          inputButtonChange.innerText = 'Отправлено!';
          inputButtonChange.setAttribute('readonly', true);
        };

        var clickToMyButton = function clickToMyButton(event) {
          _this.submitValid = false;
          Array.from(inputs).filter(filterEmpty).map(function (el) {
            return el;
          });
          if (toolsInput().length === 0) _this.submitValid = true;
          if (_this.submitValid) nextSubmit();
        };

        var changeMyInput = function changeMyInput(event) {
          if (event.target.value.length >= 2) event.target.classList.remove('warning');
        };

        var delayFunc = function delayFunc(x) {
          return Array.from(x).map(function (el) {
            el.addEventListener('click', function (event) {
              return tools.classList.remove('warning');
            });
            return false;
          });
        };

        setTimeout(function () {
          return delayFunc(selectList());
        }, 1000);
        button.addEventListener('click', function (event) {
          return clickToMyButton(event);
        });
        Array.from(inputs).forEach(function (el) {
          return el.addEventListener('input', function (event) {
            return changeMyInput(event);
          });
        });
      }
    }
  }], [{
    key: "info",
    value: function info() {
      console.log('MODULE:', this.name, true);
    }
  }]);

  return AddValidatorInput;
}();

/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29ubmVjdC5qcyIsInNvdXJjZXMiOlsid2VicGFjazovLy93ZWJwYWNrL2Jvb3RzdHJhcCIsIndlYnBhY2s6Ly8vLi9kZXZlbG9wbWVudC9jb21wb25lbnRzL2pzL2Nvbm5lY3QuanMiLCJ3ZWJwYWNrOi8vLy4vZGV2ZWxvcG1lbnQvY29tcG9uZW50cy9qcy9tb2R1bGVzL2dlbmVyYXRvclRhYmxlLmpzIiwid2VicGFjazovLy8uL2RldmVsb3BtZW50L2NvbXBvbmVudHMvanMvbW9kdWxlcy9tb3ZlU2Nyb2xsVVAuanMiLCJ3ZWJwYWNrOi8vLy4vZGV2ZWxvcG1lbnQvY29tcG9uZW50cy9qcy9tb2R1bGVzL29wZW5NeUJ1cmdlci5qcyIsIndlYnBhY2s6Ly8vLi9kZXZlbG9wbWVudC9jb21wb25lbnRzL2pzL21vZHVsZXMvcGhvbmVJbnB1dE1hc2suanMiLCJ3ZWJwYWNrOi8vLy4vZGV2ZWxvcG1lbnQvY29tcG9uZW50cy9qcy9tb2R1bGVzL3NsaWRlTXlQaWN0dXJlLmpzIiwid2VicGFjazovLy8uL2RldmVsb3BtZW50L2NvbXBvbmVudHMvanMvbW9kdWxlcy92YWxpZGF0b3JJbnB1dC5qcyJdLCJzb3VyY2VzQ29udGVudCI6WyIgXHQvLyBUaGUgbW9kdWxlIGNhY2hlXG4gXHR2YXIgaW5zdGFsbGVkTW9kdWxlcyA9IHt9O1xuXG4gXHQvLyBUaGUgcmVxdWlyZSBmdW5jdGlvblxuIFx0ZnVuY3Rpb24gX193ZWJwYWNrX3JlcXVpcmVfXyhtb2R1bGVJZCkge1xuXG4gXHRcdC8vIENoZWNrIGlmIG1vZHVsZSBpcyBpbiBjYWNoZVxuIFx0XHRpZihpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXSkge1xuIFx0XHRcdHJldHVybiBpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXS5leHBvcnRzO1xuIFx0XHR9XG4gXHRcdC8vIENyZWF0ZSBhIG5ldyBtb2R1bGUgKGFuZCBwdXQgaXQgaW50byB0aGUgY2FjaGUpXG4gXHRcdHZhciBtb2R1bGUgPSBpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXSA9IHtcbiBcdFx0XHRpOiBtb2R1bGVJZCxcbiBcdFx0XHRsOiBmYWxzZSxcbiBcdFx0XHRleHBvcnRzOiB7fVxuIFx0XHR9O1xuXG4gXHRcdC8vIEV4ZWN1dGUgdGhlIG1vZHVsZSBmdW5jdGlvblxuIFx0XHRtb2R1bGVzW21vZHVsZUlkXS5jYWxsKG1vZHVsZS5leHBvcnRzLCBtb2R1bGUsIG1vZHVsZS5leHBvcnRzLCBfX3dlYnBhY2tfcmVxdWlyZV9fKTtcblxuIFx0XHQvLyBGbGFnIHRoZSBtb2R1bGUgYXMgbG9hZGVkXG4gXHRcdG1vZHVsZS5sID0gdHJ1ZTtcblxuIFx0XHQvLyBSZXR1cm4gdGhlIGV4cG9ydHMgb2YgdGhlIG1vZHVsZVxuIFx0XHRyZXR1cm4gbW9kdWxlLmV4cG9ydHM7XG4gXHR9XG5cblxuIFx0Ly8gZXhwb3NlIHRoZSBtb2R1bGVzIG9iamVjdCAoX193ZWJwYWNrX21vZHVsZXNfXylcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubSA9IG1vZHVsZXM7XG5cbiBcdC8vIGV4cG9zZSB0aGUgbW9kdWxlIGNhY2hlXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLmMgPSBpbnN0YWxsZWRNb2R1bGVzO1xuXG4gXHQvLyBkZWZpbmUgZ2V0dGVyIGZ1bmN0aW9uIGZvciBoYXJtb255IGV4cG9ydHNcbiBcdF9fd2VicGFja19yZXF1aXJlX18uZCA9IGZ1bmN0aW9uKGV4cG9ydHMsIG5hbWUsIGdldHRlcikge1xuIFx0XHRpZighX193ZWJwYWNrX3JlcXVpcmVfXy5vKGV4cG9ydHMsIG5hbWUpKSB7XG4gXHRcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIG5hbWUsIHsgZW51bWVyYWJsZTogdHJ1ZSwgZ2V0OiBnZXR0ZXIgfSk7XG4gXHRcdH1cbiBcdH07XG5cbiBcdC8vIGRlZmluZSBfX2VzTW9kdWxlIG9uIGV4cG9ydHNcbiBcdF9fd2VicGFja19yZXF1aXJlX18uciA9IGZ1bmN0aW9uKGV4cG9ydHMpIHtcbiBcdFx0aWYodHlwZW9mIFN5bWJvbCAhPT0gJ3VuZGVmaW5lZCcgJiYgU3ltYm9sLnRvU3RyaW5nVGFnKSB7XG4gXHRcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFN5bWJvbC50b1N0cmluZ1RhZywgeyB2YWx1ZTogJ01vZHVsZScgfSk7XG4gXHRcdH1cbiBcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsICdfX2VzTW9kdWxlJywgeyB2YWx1ZTogdHJ1ZSB9KTtcbiBcdH07XG5cbiBcdC8vIGNyZWF0ZSBhIGZha2UgbmFtZXNwYWNlIG9iamVjdFxuIFx0Ly8gbW9kZSAmIDE6IHZhbHVlIGlzIGEgbW9kdWxlIGlkLCByZXF1aXJlIGl0XG4gXHQvLyBtb2RlICYgMjogbWVyZ2UgYWxsIHByb3BlcnRpZXMgb2YgdmFsdWUgaW50byB0aGUgbnNcbiBcdC8vIG1vZGUgJiA0OiByZXR1cm4gdmFsdWUgd2hlbiBhbHJlYWR5IG5zIG9iamVjdFxuIFx0Ly8gbW9kZSAmIDh8MTogYmVoYXZlIGxpa2UgcmVxdWlyZVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy50ID0gZnVuY3Rpb24odmFsdWUsIG1vZGUpIHtcbiBcdFx0aWYobW9kZSAmIDEpIHZhbHVlID0gX193ZWJwYWNrX3JlcXVpcmVfXyh2YWx1ZSk7XG4gXHRcdGlmKG1vZGUgJiA4KSByZXR1cm4gdmFsdWU7XG4gXHRcdGlmKChtb2RlICYgNCkgJiYgdHlwZW9mIHZhbHVlID09PSAnb2JqZWN0JyAmJiB2YWx1ZSAmJiB2YWx1ZS5fX2VzTW9kdWxlKSByZXR1cm4gdmFsdWU7XG4gXHRcdHZhciBucyA9IE9iamVjdC5jcmVhdGUobnVsbCk7XG4gXHRcdF9fd2VicGFja19yZXF1aXJlX18ucihucyk7XG4gXHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShucywgJ2RlZmF1bHQnLCB7IGVudW1lcmFibGU6IHRydWUsIHZhbHVlOiB2YWx1ZSB9KTtcbiBcdFx0aWYobW9kZSAmIDIgJiYgdHlwZW9mIHZhbHVlICE9ICdzdHJpbmcnKSBmb3IodmFyIGtleSBpbiB2YWx1ZSkgX193ZWJwYWNrX3JlcXVpcmVfXy5kKG5zLCBrZXksIGZ1bmN0aW9uKGtleSkgeyByZXR1cm4gdmFsdWVba2V5XTsgfS5iaW5kKG51bGwsIGtleSkpO1xuIFx0XHRyZXR1cm4gbnM7XG4gXHR9O1xuXG4gXHQvLyBnZXREZWZhdWx0RXhwb3J0IGZ1bmN0aW9uIGZvciBjb21wYXRpYmlsaXR5IHdpdGggbm9uLWhhcm1vbnkgbW9kdWxlc1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5uID0gZnVuY3Rpb24obW9kdWxlKSB7XG4gXHRcdHZhciBnZXR0ZXIgPSBtb2R1bGUgJiYgbW9kdWxlLl9fZXNNb2R1bGUgP1xuIFx0XHRcdGZ1bmN0aW9uIGdldERlZmF1bHQoKSB7IHJldHVybiBtb2R1bGVbJ2RlZmF1bHQnXTsgfSA6XG4gXHRcdFx0ZnVuY3Rpb24gZ2V0TW9kdWxlRXhwb3J0cygpIHsgcmV0dXJuIG1vZHVsZTsgfTtcbiBcdFx0X193ZWJwYWNrX3JlcXVpcmVfXy5kKGdldHRlciwgJ2EnLCBnZXR0ZXIpO1xuIFx0XHRyZXR1cm4gZ2V0dGVyO1xuIFx0fTtcblxuIFx0Ly8gT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm8gPSBmdW5jdGlvbihvYmplY3QsIHByb3BlcnR5KSB7IHJldHVybiBPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGwob2JqZWN0LCBwcm9wZXJ0eSk7IH07XG5cbiBcdC8vIF9fd2VicGFja19wdWJsaWNfcGF0aF9fXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLnAgPSBcIlwiO1xuXG5cbiBcdC8vIExvYWQgZW50cnkgbW9kdWxlIGFuZCByZXR1cm4gZXhwb3J0c1xuIFx0cmV0dXJuIF9fd2VicGFja19yZXF1aXJlX18oX193ZWJwYWNrX3JlcXVpcmVfXy5zID0gXCIuL2RldmVsb3BtZW50L2NvbXBvbmVudHMvanMvY29ubmVjdC5qc1wiKTtcbiIsIi8vID09PT09PT09PT09PT09PT09PT09PT09PT09PT1cclxuLy8gICAgTmFtZTogaW5kZXguanNcclxuLy8gPT09PT09PT09PT09PT09PT09PT09PT09PT09PVxyXG5cclxuaW1wb3J0IEFkZE1vdmVTY3JvbGxVUCBmcm9tICcuL21vZHVsZXMvbW92ZVNjcm9sbFVQJztcclxuaW1wb3J0IEFkZEdlbmVyYXRvclRhYmxlIGZyb20gJy4vbW9kdWxlcy9nZW5lcmF0b3JUYWJsZSc7XHJcbmltcG9ydCBBZGRQaG9uZUlucHV0TWFzayBmcm9tICcuL21vZHVsZXMvcGhvbmVJbnB1dE1hc2snO1xyXG5pbXBvcnQgQWRkVmFsaWRhdG9ySW5wdXQgZnJvbSAnLi9tb2R1bGVzL3ZhbGlkYXRvcklucHV0JztcclxuaW1wb3J0IEFkZFNsaWRlTXlQaWN0dXJlIGZyb20gJy4vbW9kdWxlcy9zbGlkZU15UGljdHVyZSc7XHJcbmltcG9ydCBBZGRPcGVuTXlCdXJnZXIgZnJvbSAnLi9tb2R1bGVzL29wZW5NeUJ1cmdlcic7XHJcblxyXG5jb25zdCBzdGFydCA9ICgpID0+IHtcclxuXHRjb25zb2xlLmxvZygnRE9NOicsICdET01Db250ZW50TG9hZGVkJywgdHJ1ZSk7XHJcblx0XHJcblx0bmV3IEFkZEdlbmVyYXRvclRhYmxlKHtcclxuXHRcdHNlcnZlcjogJ2VxdWlwbWVudC5qc29uJyxcclxuXHRcdHNlbGVjdG9yOiAnLmpzX19zb3J0VGFibGUnXHJcblx0fSkucnVuKCk7XHJcblx0XHJcblx0bmV3IEFkZFBob25lSW5wdXRNYXNrKHtcclxuXHRcdHNlbGVjdG9yOiAnW2RhdGEtcGhvbmU9XCJ0cnVlXCJdJyxcclxuXHRcdGlucHV0aWQ6ICcjdG9vbHMtcGhvbmUnXHJcblx0fSkucnVuKCk7XHJcblx0XHJcblx0bmV3IEFkZFZhbGlkYXRvcklucHV0KHtcclxuXHRcdHNlbGVjdG9yOiAnI3Rvb2xzLWZvcm0nXHJcblx0fSkucnVuKCk7XHJcblx0XHJcblx0bmV3IEFkZFNsaWRlTXlQaWN0dXJlKHtcclxuXHRcdHNlbGVjdG9yOiAnLmpzX19zbGlkZU15UGljdHVyZSdcclxuXHR9KS5ydW4oKTtcclxuXHRcclxuXHRuZXcgQWRkT3Blbk15QnVyZ2VyKHtcclxuXHRcdGJ1cmdlcjogJy5qc19fbmF2SGFtYnVyZ2VyJyxcclxuXHRcdG5hdmJhcjogJy5qc19fbmF2SGFtYnVyZ2VyT3BlbmVyJ1xyXG5cdH0pLnJ1bigpO1xyXG5cdFxyXG5cdG5ldyBBZGRNb3ZlU2Nyb2xsVVAoJy5qc19fbW92ZVNjcm9sbFVQJykucnVuKCk7XHJcblx0XHJcbn07XHJcblxyXG5pZiAodHlwZW9mIHdpbmRvdyAhPT0gJ3VuZGVmaW5lZCcgJiYgd2luZG93ICYmIHdpbmRvdy5hZGRFdmVudExpc3RlbmVyKSB7XHJcblx0ZG9jdW1lbnQuYWRkRXZlbnRMaXN0ZW5lcignRE9NQ29udGVudExvYWRlZCcsIHN0YXJ0KCksIGZhbHNlKTtcclxufVxyXG4gIiwibW9kdWxlLmV4cG9ydHMgPSBjbGFzcyBBZGRHZW5lcmF0b3JUYWJsZSB7XHJcblx0Y29uc3RydWN0b3IoaW5pdCkge1xyXG5cdFx0dGhpcy5zZXJ2ZXIgPSBpbml0LnNlcnZlcjtcclxuXHRcdHRoaXMuc2VsZWN0b3IgPSBpbml0LnNlbGVjdG9yO1xyXG5cdFx0dGhpcy5zb3J0Q2FyID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcignLnRvb2xzX19zb3J0LWNhcicpO1xyXG5cdFx0dGhpcy5zb3J0SGVhZGVyID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcignLnRvb2xzX19zb3J0LWhlYWRlcicpO1xyXG5cdFx0dGhpcy5lcnJvclRleHQgPSAn8J+SgCBPdWNoLCBkYXRhYmFzZSBlcnJvci4uLic7XHJcblx0fVxyXG5cclxuXHRzdGF0aWMgaW5mbygpIHtcclxuXHRcdGNvbnNvbGUubG9nKCdNT0RVTEU6JywgdGhpcy5uYW1lLCB0cnVlKTtcclxuXHR9XHJcblxyXG5cdHJ1bigpIHtcclxuXHRcdGNvbnN0IGVsZW0gPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKGAke3RoaXMuc2VsZWN0b3J9YCk7XHJcblx0XHRpZiAoZWxlbSkge1xyXG5cdFx0XHR0aGlzLmNvbnN0cnVjdG9yLmluZm8oKTtcclxuXHJcblx0XHRcdGNvbnN0IGluaXQgPSB7XHJcblx0XHRcdFx0bWV0aG9kOiAnR0VUJyxcclxuXHRcdFx0XHRoZWFkZXJzOiB7ICdDb250ZW50LVR5cGUnOiAnYXBwbGljYXRpb24vanNvbicgfSxcclxuXHRcdFx0XHRtb2RlOiAnY29ycycsXHJcblx0XHRcdFx0Y2FjaGU6ICdkZWZhdWx0J1xyXG5cdFx0XHR9O1xyXG5cclxuXHRcdFx0ZmV0Y2goYC4vJHt0aGlzLnNlcnZlcn1gLCBpbml0KVxyXG5cdFx0XHRcdC50aGVuKChyZXNwb25zZSkgPT4ge1xyXG5cdFx0XHRcdFx0cmVzcG9uc2UuaGVhZGVycy5nZXQoJ0NvbnRlbnQtVHlwZScpO1xyXG5cdFx0XHRcdFx0cmV0dXJuIHJlc3BvbnNlLmpzb24oKTtcclxuXHRcdFx0XHR9KVxyXG5cdFx0XHRcdC50aGVuKChkYXRhKSA9PiB7XHJcblx0XHRcdFx0XHRjb25zdCBteVJlbW92ZSA9IFt0aGlzLnNvcnRDYXIsIHRoaXMuc29ydEhlYWRlcl07XHJcblx0XHRcdFx0XHRteVJlbW92ZS5mb3JFYWNoKGVsID0+IChlbC50ZXh0Q29udGVudC5pbmNsdWRlcyh0aGlzLmVycm9yVGV4dCkgPyBlbC5yZW1vdmUoKSA6ICcnKSk7XHJcblxyXG5cdFx0XHRcdFx0T2JqZWN0LnZhbHVlcyhkYXRhWzBdLmNvbHVtbikubWFwKChpLCBpbmRleCkgPT4ge1xyXG5cdFx0XHRcdFx0XHRjb25zdCBjcmVhdERJViA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoJ0RJVicpO1xyXG5cdFx0XHRcdFx0XHRjcmVhdERJVi5jbGFzc05hbWUgPSAndG9vbHNfX3NvcnQtaGVhZGVyJztcclxuXHRcdFx0XHRcdFx0Y3JlYXRESVYuaW5uZXJIVE1MICs9IGkudGl0bGU7XHJcblx0XHRcdFx0XHRcdGVsZW0ucXVlcnlTZWxlY3RvcignLnRvb2xzX19zb3J0LXRpdGxlJykuYXBwZW5kQ2hpbGQoY3JlYXRESVYpO1xyXG5cdFx0XHRcdFx0XHRyZXR1cm4gZmFsc2U7XHJcblx0XHRcdFx0XHR9KTtcclxuXHJcblx0XHRcdFx0XHRPYmplY3QudmFsdWVzKGRhdGEuc2xpY2UoMSkpLmZvckVhY2goKGksIGluZGV4KSA9PiB7XHJcblx0XHRcdFx0XHRcdGNvbnN0IGNyZWF0ZUVsZW1VWCA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoJ0RJVicpO1xyXG5cdFx0XHRcdFx0XHRjcmVhdGVFbGVtVVguY2xhc3NOYW1lID0gJ3Rvb2xzX19zb3J0LWNhciB0b29sc19fc29ydC1saW5lJztcclxuXHJcblx0XHRcdFx0XHRcdE9iamVjdC52YWx1ZXMoaS5jYXIpLmZvckVhY2goKGYsIGZJbmRleCkgPT4ge1xyXG5cdFx0XHRcdFx0XHRcdGNvbnN0IGNyZWF0ZUVsZW1ESVYgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KCdESVYnKTtcclxuXHRcdFx0XHRcdFx0XHRjcmVhdGVFbGVtRElWLmNsYXNzTmFtZSA9ICd0b29sc19fc29ydC1pdGVtJztcclxuXHRcdFx0XHRcdFx0XHRjcmVhdGVFbGVtRElWLmlubmVySFRNTCArPSBmO1xyXG5cdFx0XHRcdFx0XHRcdGNyZWF0ZUVsZW1VWC5hcHBlbmRDaGlsZChjcmVhdGVFbGVtRElWKTtcclxuXHRcdFx0XHRcdFx0XHRyZXR1cm4gZmFsc2U7XHJcblx0XHRcdFx0XHRcdH0pO1xyXG5cclxuXHRcdFx0XHRcdFx0ZWxlbS5hcHBlbmRDaGlsZChjcmVhdGVFbGVtVVgpO1xyXG5cdFx0XHRcdFx0fSk7XHJcblx0XHRcdFx0fSlcclxuXHRcdFx0XHQudGhlbigoKSA9PiB7XHJcblx0XHRcdFx0XHRjb25zdCBkcm9wbGlzdCA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoJy5qc19fY3JlYXRlRHJvcExpc3QnKTtcclxuXHRcdFx0XHRcdGNvbnN0IHNlbGVjdFVsID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudCgnVUwnKTtcclxuXHRcdFx0XHRcdHNlbGVjdFVsLmNsYXNzTmFtZSA9ICdkcm9wZG93bl9faXRlbSBkcm9wZG93bl9fc2VsZWN0JztcclxuXHJcblx0XHRcdFx0XHRbLi4uZG9jdW1lbnQucXVlcnlTZWxlY3RvckFsbCgnLnRvb2xzX19zb3J0LWNhcicpXS5mb3JFYWNoKChmLCBpbmRleCkgPT4ge1xyXG5cdFx0XHRcdFx0XHRjb25zdCBzZWxlY3RMaSA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoJ0xJJyk7XHJcblx0XHRcdFx0XHRcdHNlbGVjdFVsLmFwcGVuZENoaWxkKHNlbGVjdExpKTtcclxuXHRcdFx0XHRcdFx0c2VsZWN0TGkuaW5uZXJIVE1MICs9IGYuY2hpbGRyZW5bMF0udGV4dENvbnRlbnQ7XHJcblx0XHRcdFx0XHR9KTtcclxuXHRcdFx0XHRcdGRyb3BsaXN0LmFwcGVuZENoaWxkKHNlbGVjdFVsKTtcclxuXHJcblx0XHRcdFx0fSlcclxuXHRcdFx0XHQudGhlbigoKSA9PiB7XHJcblx0XHRcdFx0XHRjb25zdCBzZWxlY3RvciA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoJ01BSU4nKTtcclxuXHRcdFx0XHRcdHNlbGVjdG9yLmFkZEV2ZW50TGlzdGVuZXIoJ2NsaWNrJywgKGV2ZW50KSA9PiB7XHJcblxyXG5cdFx0XHRcdFx0XHRkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCcuanNfX2NyZWF0ZURyb3BMaXN0IFVMJykuY2xhc3NMaXN0LnJlbW92ZSgnb3BlbicpO1xyXG5cdFx0XHRcdFx0XHRpZiAoZXZlbnQudGFyZ2V0Lm1hdGNoZXMoJy5kcm9wZG93bl9fbGFiZWwnKSkge1xyXG5cdFx0XHRcdFx0XHRcdGV2ZW50LnByZXZlbnREZWZhdWx0KCkgJiYgZXZlbnQuc3RvcFByb3BhZ2F0aW9uKCk7XHJcblx0XHRcdFx0XHRcdH1cclxuXHRcdFx0XHRcdFx0aWYgKGV2ZW50LnRhcmdldC5tYXRjaGVzKCcuanNfX2NyZWF0ZURyb3BMaXN0IElOUFVUJykpIHtcclxuXHRcdFx0XHRcdFx0XHRldmVudC50YXJnZXQucGFyZW50RWxlbWVudC5xdWVyeVNlbGVjdG9yKCdVTCcpLmNsYXNzTGlzdC5hZGQoJ29wZW4nKTtcclxuXHRcdFx0XHRcdFx0fVxyXG5cdFx0XHRcdFx0XHRpZiAoZXZlbnQudGFyZ2V0Lm5vZGVOYW1lID09PSAnTEknKSB7XHJcblx0XHRcdFx0XHRcdFx0Y29uc3QgZ3JhYlRleHQgPSBldmVudC50YXJnZXQudGV4dENvbnRlbnQ7XHJcblx0XHRcdFx0XHRcdFx0Y29uc3QgcGFzdGVJblNlbGVjdG9yID0gZXZlbnQudGFyZ2V0LnBhcmVudEVsZW1lbnQucGFyZW50RWxlbWVudDtcclxuXHRcdFx0XHRcdFx0XHRwYXN0ZUluU2VsZWN0b3IucXVlcnlTZWxlY3RvcignSU5QVVQnKS52YWx1ZSA9IGdyYWJUZXh0IHx8ICcnO1xyXG5cdFx0XHRcdFx0XHR9XHJcblxyXG5cdFx0XHRcdFx0fSk7XHJcblx0XHRcdFx0fSlcclxuXHRcdFx0XHQuY2F0Y2goZXJyb3IgPT4gY29uc29sZS5sb2coYE91Y2ghIEZldGNoIGVycm9yOiBcXG4gLS0+ICR7ZXJyb3IubWVzc2FnZX1gKSk7XHJcblx0XHR9XHJcblx0fVxyXG59O1xyXG4iLCJtb2R1bGUuZXhwb3J0cyA9IGNsYXNzIEFkZE1vdmVTY3JvbGxVUCB7XHJcblx0Y29uc3RydWN0b3IoaXRlbSwgc3BlZWQpIHtcclxuXHRcdHRoaXMuaXRlbSA9IGl0ZW07XHJcblx0XHR0aGlzLnNwZWVkID0gc3BlZWQ7XHJcblx0fVxyXG5cclxuXHRzdGF0aWMgaW5mbygpIHtcclxuXHRcdGNvbnNvbGUubG9nKCdNT0RVTEU6JywgdGhpcy5uYW1lLCB0cnVlKTtcclxuXHR9XHJcblxyXG5cdHJ1bigpIHtcclxuXHRcdGNvbnN0IGVsZW0gPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKGAke3RoaXMuaXRlbX1gKTtcclxuXHRcdGlmIChlbGVtKSB7XHJcblx0XHRcdHRoaXMuY29uc3RydWN0b3IuaW5mbygpO1xyXG5cdFx0XHR0aGlzLnNwZWVkID0gdGhpcy5zcGVlZCA9PT0gJ2Zhc3QnID8gOCAvIDIgOiA4O1xyXG5cdFx0XHR0aGlzLnNwZWVkID0gdGhpcy5zcGVlZCA9PT0gJ3Nsb3cnID8gOCAqIDIgOiA4O1xyXG5cclxuXHRcdFx0Y29uc3Qgc2Nyb2xsTWUgPSAoKSA9PiB7XHJcblx0XHRcdFx0Y29uc3QgZ2V0U2Nyb2xsID0gZG9jdW1lbnQuZG9jdW1lbnRFbGVtZW50LnNjcm9sbFRvcCB8fCBkb2N1bWVudC5ib2R5LnNjcm9sbFRvcDtcclxuXHRcdFx0XHRpZiAoZ2V0U2Nyb2xsID4gMCkge1xyXG5cdFx0XHRcdFx0d2luZG93LnJlcXVlc3RBbmltYXRpb25GcmFtZShzY3JvbGxNZSk7XHJcblx0XHRcdFx0XHR3aW5kb3cuc2Nyb2xsVG8oMCwgZ2V0U2Nyb2xsIC0gZ2V0U2Nyb2xsIC8gdGhpcy5zcGVlZCk7XHJcblx0XHRcdFx0fVxyXG5cdFx0XHR9O1xyXG5cdFx0XHRlbGVtLmFkZEV2ZW50TGlzdGVuZXIoJ2NsaWNrJywgKCkgPT4gc2Nyb2xsTWUoKSk7XHJcblx0XHR9XHJcblx0fVxyXG59O1xyXG4iLCJtb2R1bGUuZXhwb3J0cyA9IGNsYXNzIEFkZE9wZW5NeUJ1cmdlciB7XHJcblx0Y29uc3RydWN0b3IoaW5pdCkge1xyXG5cdFx0dGhpcy5zZWxlY3RvciA9IGluaXQuYnVyZ2VyO1xyXG5cdFx0dGhpcy5uYXZiYXIgPSBpbml0Lm5hdmJhcjtcclxuXHRcdHRoaXMuZXJyb3JUZXh0ID0gJ/CfkoAgT3VjaCwgZGF0YWJhc2UgZXJyb3IuLi4nO1xyXG5cdH1cclxuXHJcblx0c3RhdGljIGluZm8oKSB7XHJcblx0XHRjb25zb2xlLmxvZygnTU9EVUxFOicsIHRoaXMubmFtZSwgdHJ1ZSk7XHJcblx0fVxyXG5cclxuXHRydW4oKSB7XHJcblx0XHRjb25zdCBzZWxlY3RvciA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoYCR7dGhpcy5zZWxlY3Rvcn1gKTtcclxuXHRcdGlmIChzZWxlY3Rvcikge1xyXG5cdFx0XHR0aGlzLmNvbnN0cnVjdG9yLmluZm8oKTtcclxuXHJcblx0XHRcdGNvbnN0IG9wZW5CdXJnZXIgPSAoKSA9PiB7XHJcblx0XHRcdFx0Y29uc3QgbmF2ID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcihgJHt0aGlzLm5hdmJhcn1gKTtcclxuXHRcdFx0XHRuYXYucXVlcnlTZWxlY3RvcignRElWJykuY2xhc3NMaXN0LnRvZ2dsZSgnb3BlbicpO1xyXG5cdFx0XHRcdHNlbGVjdG9yLmNsYXNzTGlzdC50b2dnbGUoJ29wZW4nKTtcclxuXHRcdFx0fTtcclxuXHJcblx0XHRcdHNlbGVjdG9yLmFkZEV2ZW50TGlzdGVuZXIoJ2NsaWNrJywgKCkgPT4gb3BlbkJ1cmdlcigpKTtcclxuXHRcdH1cclxuXHR9XHJcbn07XHJcbiIsIm1vZHVsZS5leHBvcnRzID0gY2xhc3MgQWRkUGhvbmVJbnB1dE1hc2sge1xyXG5cdGNvbnN0cnVjdG9yKGluaXQpIHtcclxuXHRcdHRoaXMuc2VsZWN0b3IgPSBpbml0LnNlbGVjdG9yO1xyXG5cdFx0dGhpcy5pbnB1dCA9IGluaXQuaW5wdXRpZDtcclxuXHR9XHJcblxyXG5cdHN0YXRpYyBpbmZvKCkge1xyXG5cdFx0Y29uc29sZS5sb2coJ01PRFVMRTonLCB0aGlzLm5hbWUsIHRydWUpO1xyXG5cdH1cclxuXHJcblx0cnVuKCkge1xyXG5cdFx0Y29uc3QgZWxlbSA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoYCR7dGhpcy5zZWxlY3Rvcn1gKTtcclxuXHRcdGlmIChlbGVtKSB7XHJcblx0XHRcdHRoaXMuY29uc3RydWN0b3IuaW5mbygpO1xyXG5cdFx0XHRjb25zdCBzZWxlY3QgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKGAke3RoaXMuaW5wdXR9YCk7XHJcblx0XHRcdGNvbnN0IG15ZnVuYyA9IChldmVudCkgPT4ge1xyXG5cdFx0XHRcdGNvbnN0IHJlZ2V4cCA9IC9eXFwrP1swLTldezAsMX1cXHM/WyhdezAsMX1bMC05XXswLDN9WyldezAsMX1bLVxccy4vMC05XSokLztcclxuXHRcdFx0XHRpZiAoIXNlbGVjdC52YWx1ZS5tYXRjaChyZWdleHApKSBzZWxlY3QudmFsdWUgPSBzZWxlY3QudmFsdWUuc2xpY2UoMCwgLTEpO1xyXG5cdFx0XHR9O1xyXG5cclxuXHRcdFx0c2VsZWN0LmFkZEV2ZW50TGlzdGVuZXIoJ2tleXVwJywgZXZlbnQgPT4gbXlmdW5jKGV2ZW50KSk7XHJcblx0XHR9XHJcblx0fVxyXG59O1xyXG4iLCJtb2R1bGUuZXhwb3J0cyA9IGNsYXNzIEFkZFNsaWRlTXlQaWN0dXJlIHtcclxuXHRjb25zdHJ1Y3Rvcihpbml0KSB7XHJcblx0XHR0aGlzLnNlcnZlciA9IGluaXQuc2VydmVyO1xyXG5cdFx0dGhpcy5zZWxlY3RvciA9IGluaXQuc2VsZWN0b3I7XHJcblx0XHR0aGlzLmluZGV4TmF2ID0gMDtcclxuXHRcdHRoaXMuaW5kZXhOYXZNaW4gPSAwO1xyXG5cdFx0dGhpcy5pbmRleE5hdk1heCA9IDA7XHJcblx0XHR0aGlzLnRpbWVySW50ZXJ2YWwgPSBudWxsO1xyXG5cdFx0dGhpcy5lcnJvclRleHQgPSAn8J+SgCBPdWNoLCBkYXRhYmFzZSBlcnJvci4uLic7XHJcblx0fVxyXG5cclxuXHRzdGF0aWMgaW5mbygpIHtcclxuXHRcdGNvbnNvbGUubG9nKCdNT0RVTEU6JywgdGhpcy5uYW1lLCB0cnVlKTtcclxuXHR9XHJcblxyXG5cdHJ1bigpIHtcclxuXHRcdGNvbnN0IGVsZW0gPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKGAke3RoaXMuc2VsZWN0b3J9YCk7XHJcblx0XHRpZiAoZWxlbSkge1xyXG5cdFx0XHR0aGlzLmNvbnN0cnVjdG9yLmluZm8oKTtcclxuXHJcblx0XHRcdGNvbnN0IHNlbGVjdG9yID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcignLmpzX19zbGlkZU15UGljdHVyZU5hdmJhcicpO1xyXG5cdFx0XHRjb25zdCBteVJlbW92ZSA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoJy5qc19fc2xpZGVNeVBpY3R1cmVOYXZiYXIgc3BhbicpO1xyXG5cdFx0XHRjb25zdCBidXR0b25SaWdodCA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoJy5nYWxsZXJ5X19hcnJvdy1yaWdodCcpO1xyXG5cdFx0XHRjb25zdCBidXR0b25MZWZ0ID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcignLmdhbGxlcnlfX2Fycm93LWxlZnQnKTtcclxuXHRcdFx0Y29uc3QgY2xpY2tQbGF5ID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcignLmdhbGxlcnlfX3BsYXktYnV0dG9uJyk7XHJcblx0XHRcdGNvbnN0IHN0b3BDbGlja2xQbGF5ID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcignLmdhbGxlcnlfX3BsYXktc3RvcHBlcicpO1xyXG5cclxuXHRcdFx0aWYgKG15UmVtb3ZlLnRleHRDb250ZW50LmluY2x1ZGVzKHRoaXMuZXJyb3JUZXh0KSkgbXlSZW1vdmUucmVtb3ZlKCk7XHJcblxyXG5cdFx0XHR0aGlzLmluZGV4TmF2TWF4ID0gQXJyYXkuZnJvbShkb2N1bWVudC5xdWVyeVNlbGVjdG9yQWxsKCcuanNfX3NsaWRlTXlQaWN0dXJlIElNRycpKS5sZW5ndGggLSAxO1xyXG5cclxuXHRcdFx0QXJyYXkuZnJvbShkb2N1bWVudC5xdWVyeVNlbGVjdG9yQWxsKCcuanNfX3NsaWRlTXlQaWN0dXJlIElNRycpKS5tYXAoKGVsKSA9PiB7XHJcblx0XHRcdFx0Y29uc3QgY3JlYXRlRElWID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudCgnRElWJyk7XHJcblx0XHRcdFx0Y3JlYXRlRElWLmNsYXNzTGlzdC5hZGQoJ2dhbGxlcnlfX25hdmJhci1kb3RzJyk7XHJcblx0XHRcdFx0Y3JlYXRlRElWLmlubmVySFRNTCArPSAnJm5ic3A7JztcclxuXHRcdFx0XHRyZXR1cm4gc2VsZWN0b3IuYXBwZW5kQ2hpbGQoY3JlYXRlRElWKTtcclxuXHRcdFx0fSk7XHJcblxyXG5cdFx0XHRzZWxlY3Rvci5maXJzdEVsZW1lbnRDaGlsZC5jbGFzc0xpc3QuYWRkKCdhY3RpdmUnKTtcclxuXHJcblx0XHRcdGNvbnN0IGxvZ2ljID0gKGV2ZW50KSA9PiB7XHJcblx0XHRcdFx0a2lsbE15VGltZXIodGhpcy50aW1lckludGVydmFsKTtcclxuXHRcdFx0XHRjb25zdCBjdXJFbGVtID0gZXZlbnQudGFyZ2V0O1xyXG5cdFx0XHRcdHRoaXMuaW5kZXhOYXYgPSBbLi4uY3VyRWxlbS5wYXJlbnRFbGVtZW50LmNoaWxkcmVuXS5pbmRleE9mKGN1ckVsZW0pO1xyXG5cdFx0XHRcdGlmIChjdXJFbGVtLmNsYXNzTGlzdC5jb250YWlucygnYWN0aXZlJykpIHJldHVybiBmYWxzZTtcclxuXHRcdFx0XHRpZiAoc2VsZWN0b3IucXVlcnlTZWxlY3RvcignLmFjdGl2ZScpKSB7XHJcblx0XHRcdFx0XHRzZWxlY3Rvci5xdWVyeVNlbGVjdG9yKCcuYWN0aXZlJykuY2xhc3NMaXN0LnJlbW92ZSgnYWN0aXZlJyk7XHJcblx0XHRcdFx0fVxyXG5cdFx0XHRcdGN1cnJlbnRJbmRleCgpO1xyXG5cdFx0XHRcdHJldHVybiBzZWxlY3Rvci5jaGlsZHJlblt0aGlzLmluZGV4TmF2XS5jbGFzc0xpc3QuYWRkKCdhY3RpdmUnKTtcclxuXHRcdFx0fTtcclxuXHJcblx0XHRcdGNvbnN0IE1vdmVOYXZEb3RzID0gKCkgPT4ge1xyXG5cdFx0XHRcdHNlbGVjdG9yLnF1ZXJ5U2VsZWN0b3IoJy5hY3RpdmUnKS5jbGFzc0xpc3QucmVtb3ZlKCdhY3RpdmUnKTtcclxuXHRcdFx0XHRzZWxlY3Rvci5jaGlsZHJlblt0aGlzLmluZGV4TmF2XS5jbGFzc0xpc3QuYWRkKCdhY3RpdmUnKTtcclxuXHRcdFx0fTtcclxuXHRcdFx0XHJcblx0XHRcdGNvbnN0IGNvdW50SW5kZXhDbGFzc0xpc3QgPSAoKSA9PiB7XHJcblx0XHRcdFx0ZWxlbS5xdWVyeVNlbGVjdG9yKCcuYWN0aXZlJykuY2xhc3NMaXN0LnJlbW92ZSgnYWN0aXZlJyk7XHJcblx0XHRcdFx0aWYgKGVsZW0uY2hpbGRyZW5bdGhpcy5pbmRleE5hdl0uY2xhc3NMaXN0LmNvbnRhaW5zKCdhY3RpdmUnKSkge1xyXG5cdFx0XHRcdFx0ZWxlbS5jaGlsZHJlblt0aGlzLmluZGV4TmF2XS5yZW1vdmUoJ2FjdGl2ZScpO1xyXG5cdFx0XHRcdH1cclxuXHRcdFx0fTtcclxuXHRcdFx0XHJcblx0XHRcdGNvbnN0IGN1cnJlbnRJbmRleCA9ICgpID0+IHtcclxuXHRcdFx0XHRjb3VudEluZGV4Q2xhc3NMaXN0KCk7XHJcblx0XHRcdFx0ZWxlbS5jaGlsZHJlblt0aGlzLmluZGV4TmF2XS5jbGFzc0xpc3QuYWRkKCdhY3RpdmUnKTtcclxuXHRcdFx0fTtcclxuXHJcblx0XHRcdGNvbnN0IG1vdmVJblJpZ2h0ID0gKCkgPT4ge1xyXG5cdFx0XHRcdGlmICh0aGlzLmluZGV4TmF2ID49IHRoaXMuaW5kZXhOYXZNYXgpIHJldHVybiBmYWxzZTtcclxuXHRcdFx0XHRjb3VudEluZGV4Q2xhc3NMaXN0KCk7XHJcblx0XHRcdFx0ZWxlbS5jaGlsZHJlblt0aGlzLmluZGV4TmF2ICs9IDFdLmNsYXNzTGlzdC5hZGQoJ2FjdGl2ZScpO1xyXG5cdFx0XHRcdHJldHVybiBNb3ZlTmF2RG90cygpO1xyXG5cdFx0XHR9O1xyXG5cclxuXHRcdFx0Y29uc3QgbW92ZUluTGVmdCA9ICgpID0+IHtcclxuXHRcdFx0XHRpZiAodGhpcy5pbmRleE5hdiA8PSB0aGlzLmluZGV4TmF2TWluKSByZXR1cm4gZmFsc2U7XHJcblx0XHRcdFx0Y291bnRJbmRleENsYXNzTGlzdCgpO1xyXG5cdFx0XHRcdGVsZW0uY2hpbGRyZW5bdGhpcy5pbmRleE5hdiArPSAtMV0uY2xhc3NMaXN0LmFkZCgnYWN0aXZlJyk7XHJcblx0XHRcdFx0cmV0dXJuIE1vdmVOYXZEb3RzKCk7XHJcblx0XHRcdH07XHJcblxyXG5cdFx0XHRjb25zdCB0aW1lckZuYyA9ICgpID0+IHtcclxuXHRcdFx0XHRpZiAodGhpcy5pbmRleE5hdiA+PSB0aGlzLmluZGV4TmF2TWF4KSB7XHJcblx0XHRcdFx0XHR0aGlzLmluZGV4TmF2ID0gMTtcclxuXHRcdFx0XHRcdG1vdmVJbkxlZnQoKTtcclxuXHRcdFx0XHR9IGVsc2UgbW92ZUluUmlnaHQoKTtcclxuXHRcdFx0fTtcclxuXHJcblx0XHRcdGNsaWNrUGxheS5hZGRFdmVudExpc3RlbmVyKCdjbGljaycsICgpID0+IHtcclxuXHRcdFx0XHRjbGlja1BsYXkuc3R5bGUub3BhY2l0eSA9IDA7XHJcblx0XHRcdFx0Y2xpY2tQbGF5LnN0eWxlLnZpc2liaWxpdHkgPSAnaGlkZGVuJztcclxuXHRcdFx0XHRzdG9wQ2xpY2tsUGxheS5zdHlsZS52aXNpYmlsaXR5ID0gJ3Zpc2libGUnO1xyXG5cdFx0XHRcdHRoaXMudGltZXJJbnRlcnZhbCA9IHNldEludGVydmFsKCgpID0+IHRpbWVyRm5jKCksIDEwMDApO1xyXG5cdFx0XHR9KTtcclxuXHJcblx0XHRcdGNvbnN0IGtpbGxNeVRpbWVyID0gKHgpID0+IHtcclxuXHRcdFx0XHRjbGVhckludGVydmFsKHgpO1xyXG5cdFx0XHRcdGNsaWNrUGxheS5zdHlsZS5vcGFjaXR5ID0gMTtcclxuXHRcdFx0XHRjbGlja1BsYXkuc3R5bGUudmlzaWJpbGl0eSA9ICd2aXNpYmxlJztcclxuXHRcdFx0XHRzdG9wQ2xpY2tsUGxheS5zdHlsZS52aXNpYmlsaXR5ID0gJyc7XHJcblx0XHRcdH07XHJcblxyXG5cdFx0XHRzdG9wQ2xpY2tsUGxheS5hZGRFdmVudExpc3RlbmVyKCdjbGljaycsICgpID0+IGtpbGxNeVRpbWVyKHRoaXMudGltZXJJbnRlcnZhbCkpO1xyXG5cdFx0XHRzZWxlY3Rvci5hZGRFdmVudExpc3RlbmVyKCdjbGljaycsIGV2ZW50ID0+IGxvZ2ljKGV2ZW50KSk7XHJcblx0XHRcdGJ1dHRvblJpZ2h0LmFkZEV2ZW50TGlzdGVuZXIoJ2NsaWNrJywgKCkgPT4gbW92ZUluUmlnaHQoKSk7XHJcblx0XHRcdGJ1dHRvbkxlZnQuYWRkRXZlbnRMaXN0ZW5lcignY2xpY2snLCAoKSA9PiBtb3ZlSW5MZWZ0KCkpO1xyXG5cclxuXHRcdH1cclxuXHR9XHJcbn07XHJcbiIsIm1vZHVsZS5leHBvcnRzID0gY2xhc3MgQWRkVmFsaWRhdG9ySW5wdXQge1xyXG5cdGNvbnN0cnVjdG9yKGluaXQpIHtcclxuXHRcdHRoaXMuc2VydmVyID0gaW5pdC5zZXJ2ZXI7XHJcblx0XHR0aGlzLnNlbGVjdG9yID0gaW5pdC5zZWxlY3RvcjtcclxuXHRcdHRoaXMuc3VibWl0VmFsaWQgPSBmYWxzZTtcclxuXHRcdHRoaXMuZXJyb3JUZXh0ID0gJ/CfkoAgT3VjaCwgZGF0YWJhc2UgZXJyb3IuLi4nO1xyXG5cdH1cclxuXHJcblx0c3RhdGljIGluZm8oKSB7XHJcblx0XHRjb25zb2xlLmxvZygnTU9EVUxFOicsIHRoaXMubmFtZSwgdHJ1ZSk7XHJcblx0fVxyXG5cclxuXHRydW4oKSB7XHJcblx0XHRjb25zdCBlbGVtID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcihgJHt0aGlzLnNlbGVjdG9yfWApO1xyXG5cdFx0aWYgKGVsZW0pIHtcclxuXHRcdFx0dGhpcy5jb25zdHJ1Y3Rvci5pbmZvKCk7XHJcblxyXG5cdFx0XHRjb25zdCB0b29scyA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoJyN0b29scy1zZWxlY3QnKTtcclxuXHRcdFx0Y29uc3QgdG9vbHNJbnB1dCA9ICgpID0+IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3JBbGwoJy50b29sc19faW5wdXQud2FybmluZycpO1xyXG5cdFx0XHRjb25zdCBidXR0b24gPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCcjdG9vbHMtZm9ybSBidXR0b24nKTtcclxuXHRcdFx0Y29uc3QgaW5wdXRzID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvckFsbCgnI3Rvb2xzLWZvcm0gaW5wdXQnKTtcclxuXHRcdFx0Y29uc3Qgc2VsZWN0TGlzdCA9ICgpID0+IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3JBbGwoJy5kcm9wZG93bl9fc2VsZWN0IGxpJyk7XHJcblxyXG5cdFx0XHRjb25zdCBmaWx0ZXJFbXB0eSA9IChpdGVtKSA9PiB7XHJcblx0XHRcdFx0aWYgKGl0ZW0udmFsdWUubGVuZ3RoIDw9IDIpIGl0ZW0uY2xhc3NMaXN0LmFkZCgnd2FybmluZycpO1xyXG5cdFx0XHR9O1xyXG5cclxuXHRcdFx0Y29uc3QgbmV4dFN1Ym1pdCA9ICgpID0+IHtcclxuXHRcdFx0XHRjb25zb2xlLmxvZygn0J7RgtC/0YDQsNCy0LvQtdC90L4hJyk7XHJcblx0XHRcdFx0Wy4uLmRvY3VtZW50LnF1ZXJ5U2VsZWN0b3JBbGwoJy50b29sc19faW5wdXQnKV0uZm9yRWFjaCgoZWwpID0+IHtcclxuXHRcdFx0XHRcdGNvbnN0IGN1cnJlbnRFbGVtID0gZWw7XHJcblx0XHRcdFx0XHRjdXJyZW50RWxlbS5zdHlsZS5iYWNrZ3JvdW5kQ29sb3IgPSAnI2RkZCc7XHJcblx0XHRcdFx0XHRjdXJyZW50RWxlbS5zdHlsZS5wb2ludGVyRXZlbnRzID0gJ25vbmUnO1xyXG5cdFx0XHRcdFx0Y3VycmVudEVsZW0uc2V0QXR0cmlidXRlKCdyZWFkb25seScsIHRydWUpO1xyXG5cdFx0XHRcdH0pO1xyXG5cclxuXHRcdFx0XHRjb25zdCBpbnB1dEJ1dHRvbkNoYW5nZSA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoJy50b29sc19fYnV0dG9uJyk7XHJcblx0XHRcdFx0aW5wdXRCdXR0b25DaGFuZ2Uuc3R5bGUucG9pbnRlckV2ZW50cyA9ICdub25lJztcclxuXHRcdFx0XHRpbnB1dEJ1dHRvbkNoYW5nZS5zdHlsZS5iYWNrZ3JvdW5kQ29sb3IgPSAnIzVjY2MyYyc7XHJcblx0XHRcdFx0aW5wdXRCdXR0b25DaGFuZ2UuaW5uZXJUZXh0ID0gJ9Ce0YLQv9GA0LDQstC70LXQvdC+ISc7XHJcblx0XHRcdFx0aW5wdXRCdXR0b25DaGFuZ2Uuc2V0QXR0cmlidXRlKCdyZWFkb25seScsIHRydWUpO1xyXG5cdFx0XHR9O1xyXG5cclxuXHRcdFx0Y29uc3QgY2xpY2tUb015QnV0dG9uID0gKGV2ZW50KSA9PiB7XHJcblx0XHRcdFx0dGhpcy5zdWJtaXRWYWxpZCA9IGZhbHNlO1xyXG5cdFx0XHRcdEFycmF5LmZyb20oaW5wdXRzKS5maWx0ZXIoZmlsdGVyRW1wdHkpLm1hcChlbCA9PiBlbCk7XHJcblx0XHRcdFx0aWYgKHRvb2xzSW5wdXQoKS5sZW5ndGggPT09IDApIHRoaXMuc3VibWl0VmFsaWQgPSB0cnVlO1xyXG5cdFx0XHRcdGlmICh0aGlzLnN1Ym1pdFZhbGlkKSBuZXh0U3VibWl0KCk7XHJcblx0XHRcdH07XHJcblxyXG5cdFx0XHRjb25zdCBjaGFuZ2VNeUlucHV0ID0gKGV2ZW50KSA9PiB7XHJcblx0XHRcdFx0aWYgKGV2ZW50LnRhcmdldC52YWx1ZS5sZW5ndGggPj0gMikgZXZlbnQudGFyZ2V0LmNsYXNzTGlzdC5yZW1vdmUoJ3dhcm5pbmcnKTtcclxuXHRcdFx0fTtcclxuXHJcblx0XHRcdGNvbnN0IGRlbGF5RnVuYyA9IHggPT4gQXJyYXkuZnJvbSh4KS5tYXAoKGVsKSA9PiB7XHJcblx0XHRcdFx0ZWwuYWRkRXZlbnRMaXN0ZW5lcignY2xpY2snLCBldmVudCA9PiB0b29scy5jbGFzc0xpc3QucmVtb3ZlKCd3YXJuaW5nJykpO1xyXG5cdFx0XHRcdHJldHVybiBmYWxzZTtcclxuXHRcdFx0fSk7XHJcblxyXG5cdFx0XHRzZXRUaW1lb3V0KCgpID0+IGRlbGF5RnVuYyhzZWxlY3RMaXN0KCkpLCAxMDAwKTtcclxuXHJcblx0XHRcdGJ1dHRvbi5hZGRFdmVudExpc3RlbmVyKCdjbGljaycsIGV2ZW50ID0+IGNsaWNrVG9NeUJ1dHRvbihldmVudCkpO1xyXG5cdFx0XHRBcnJheS5mcm9tKGlucHV0cykuZm9yRWFjaChlbCA9PiBlbC5hZGRFdmVudExpc3RlbmVyKCdpbnB1dCcsIGV2ZW50ID0+IGNoYW5nZU15SW5wdXQoZXZlbnQpKSk7XHJcblx0XHR9XHJcblx0fVxyXG59O1xyXG4iXSwibWFwcGluZ3MiOiI7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7Ozs7Ozs7O0FDbEZBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBRkE7QUFLQTtBQUNBO0FBQ0E7QUFGQTtBQUtBO0FBQ0E7QUFEQTtBQUlBO0FBQ0E7QUFEQTtBQUlBO0FBQ0E7QUFDQTtBQUZBO0FBS0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDM0NBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBUkE7QUFBQTtBQUFBO0FBYUE7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFKQTtBQU9BO0FBRUE7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUFBO0FBQUE7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUVBO0FBRUE7QUFDQTtBQUVBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQTNGQTtBQUFBO0FBQUE7QUFVQTtBQUNBO0FBWEE7QUFDQTtBQURBO0FBQUE7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFMQTtBQUFBO0FBQUE7QUFVQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBMUJBO0FBQUE7QUFBQTtBQU9BO0FBQ0E7QUFSQTtBQUNBO0FBREE7QUFBQTs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBTkE7QUFBQTtBQUFBO0FBV0E7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBeEJBO0FBQUE7QUFBQTtBQVFBO0FBQ0E7QUFUQTtBQUNBO0FBREE7QUFBQTs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUxBO0FBQUE7QUFBQTtBQVdBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBdEJBO0FBQUE7QUFBQTtBQU9BO0FBQ0E7QUFSQTtBQUNBO0FBREE7QUFBQTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ0FBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQVZBO0FBQUE7QUFBQTtBQWVBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBRUE7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUVBO0FBQ0E7QUE5R0E7QUFBQTtBQUFBO0FBWUE7QUFDQTtBQWJBO0FBQ0E7QUFEQTtBQUFBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBUEE7QUFBQTtBQUFBO0FBWUE7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBRUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBSEE7QUFDQTtBQUlBO0FBQUE7QUFBQTtBQUVBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBaEVBO0FBQUE7QUFBQTtBQVNBO0FBQ0E7QUFWQTtBQUNBO0FBREE7QUFBQTs7OztBIiwic291cmNlUm9vdCI6IiJ9